CREATE TABLE task(
   id INT PRIMARY KEY     NOT NULL,
   name           TEXT    NOT NULL,
   checked        boolean
);


INSERT INTO task (id, name, checked) values (1,'first task',false);
INSERT INTO task (id, name, checked) values (2,'second task',false);
INSERT INTO task (id, name, checked) values (3,'third task',true);

